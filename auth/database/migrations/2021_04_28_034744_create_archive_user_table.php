<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArchiveUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archive_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('archived_user_id'); // user_id yang dihapus/arsipkan
            $table->string('notes');
            $table->unsignedBigInteger('user_id'); // pelaku
            $table->timestamps();
        });

        Schema::create('reactivation_user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('archived_user_id'); // user_id yang diaktifkan
            $table->string('notes');
            $table->unsignedBigInteger('user_id'); // pelaku
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reactivation_user');
        Schema::dropIfExists('archive_user');
    }
}
