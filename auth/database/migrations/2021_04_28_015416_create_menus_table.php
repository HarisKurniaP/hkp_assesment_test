<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('desciption');
            $table->timestamps();
        });

        Schema::create('access_controls', function (Blueprint $table) {
            $table->bigIncrements('id'); // index, show, create, store, edit, update, destroy
            $table->string('name');
            $table->string('description');
            $table->timestamps();
        });


        Schema::create('access_menu_role', function (Blueprint $table) {
            $table->unsignedBigInteger('access_id');
            $table->unsignedBigInteger('menu_id');
            $table->unsignedBigInteger('role_id');
            $table->timestamps();

            $table->foreign('access_id')
                ->references('id')
                ->on('access_controls')
                ->onUpdate('cascade')
                ->onDelete('restrict')
                ;


            $table->foreign('menu_id')
                ->references('id')
                ->on('menus')
                ->onUpdate('cascade')
                ->onDelete('restrict')
                ;

            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onUpdate('cascade')
                ->onDelete('restrict')
                ;
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_menu_role');
        Schema::dropIfExists('access_controls');
        Schema::dropIfExists('menus');
    }
}
