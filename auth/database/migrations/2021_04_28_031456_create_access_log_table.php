<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_access_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('year')->index();
            $table->string('url');
            $table->string('ip_address');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('restrict')
            ;
        });

        Schema::create('internal_api_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('year')->index();
            $table->string('endpoint')->index();
            $table->string('ip_address');
            $table->string('request_id')->index(); // GUID
            $table->string('request');
            $table->string('response');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onUpdate('cascade')
            ->onDelete('restrict')
            ;
        });

        Schema::create('external_api_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('year')->index();
            $table->string('endpoint')->index();
            $table->string('source_ip_address'); //
            $table->string('destination_ip_address'); //
            $table->string('direction'); // inbound , outbound
            $table->string('request_id')->index();
            $table->string('request');
            $table->string('response');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

        });

        Schema::create('proxy_api_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('year')->index();
            $table->string('endpoint')->index();
            $table->string('source_ip_address'); //
            $table->string('destination_ip_address'); //
            $table->string('direction'); // inbound , outbound
            $table->string('request_id')->index();
            $table->string('request');
            $table->string('response');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

        });


    }

    // User -> [frontend] klik Detail -> [backend] client/get -> [proxy] client/get  ->  [eksternal] spectra/get
    // User -> [frontend] klik Detail -> [backend] client/get -> [proxy] client/get  --> DB::QUERY
    // log seperti apa ?
    // api_access_log -- IP ADDRESS dari frontend
    // external_api_log -- IP ADDRESS dari frontend


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proxy_api_log');
        Schema::dropIfExists('external_api_log');
        Schema::dropIfExists('internal_api_log');
        Schema::dropIfExists('menu_access_log');
    }
}
