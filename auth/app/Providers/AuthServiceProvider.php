<?php

namespace App\Providers;

use App\Models\InternalApiLog;
use App\Models\UnauthorizedLog;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class AuthServiceProvider extends ServiceProvider
{
    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
           $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'IP tidak dikenali';

        return $ipaddress;
    }

    public function UUID()
    {
        $id_respon =Uuid::uuid4()->toString();
        return $id_respon;
    }

    public function MakeUrl($url,$path,$modul)
    {
        $explode_url    = explode("/", $url);
        $url_log        = $explode_url[0] . '//' . $explode_url[2] . '/'.$modul.'/' . $path;

        return $url_log;
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.

        // $this->app['auth']->viaRequest('api', function ($request) {
        //     if ($request->input('api_token')) {
        //         return User::where('api_token', $request->input('api_token'))->first();
        //     }
        // });

        $this->app['auth']->viaRequest('api', function ($request) {

            if ($request->header('Authorization')) {
            $apiToken = explode(' ', $request->header('Authorization'));
            $id = DB::table('users')
            ->where('remember_token', $apiToken[1])
            ->first();
            if(isset($id) && !empty($id))
                {
                    return DB::table('users')
                    ->select(['users.id', 'users.remember_token', 'oauth_access_tokens.user_id', 'oauth_access_tokens.revoked'])
                    ->join('oauth_access_tokens', 'oauth_access_tokens.user_id', '=', 'users.id')
                    ->where('users.remember_token', $apiToken[1])
                    ->where('oauth_access_tokens.user_id', $id->id)
                    ->where('oauth_access_tokens.revoked', 0)->first();
                }
            }

            });

    }
}
