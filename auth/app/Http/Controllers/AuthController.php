<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Response as HttpResponse;

class AuthController extends Controller
{

        public function getUserID(string $bearerToken)
        {

                $findUser   = User::where('remember_token', $bearerToken)->first();
                if ($findUser) {
                        return $findUser->id;
                } else {
                        return 0;
                }
        }

        function get_client_ip()
        {
                $ipaddress = '';
                if (getenv('HTTP_CLIENT_IP'))
                        $ipaddress = getenv('HTTP_CLIENT_IP');
                else if (getenv('HTTP_X_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
                else if (getenv('HTTP_X_FORWARDED'))
                        $ipaddress = getenv('HTTP_X_FORWARDED');
                else if (getenv('HTTP_FORWARDED_FOR'))
                        $ipaddress = getenv('HTTP_FORWARDED_FOR');
                else if (getenv('HTTP_FORWARDED'))
                        $ipaddress = getenv('HTTP_FORWARDED');
                else if (getenv('REMOTE_ADDR'))
                        $ipaddress = getenv('REMOTE_ADDR');
                else
                        $ipaddress = 'IP tidak dikenali';

                return $ipaddress;
        }

        public function UUID()
        {
                $id_respon = Uuid::uuid4()->toString();
                return $id_respon;
        }

        public function login(Request $request)
        {
                $input = $request->all();
                $response_id = Uuid::uuid4()->toString();
                $validator = Validator::make($request->all(), [
                        'username'      => 'required',
                        'password'      => 'required',
                        'request_id'    => 'required'
                ]);

                if ($validator->fails()) {
                        return response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                try {


                $check_user = User::where('username',$input['username'])->first();
                if($check_user)
                {
                        if (!Hash::check($request->password, $check_user->password)) {
                                $response = [
                                        'response_code'     => "99",
                                        'response_message'  => 'Failed'
                                ];
                                $http_respon = Response::HTTP_OK;
                        } else {
                                $token = $check_user->createToken('nApp')->accessToken;
                                $check_user->update([
                                        'remember_token' => $token
                                ]);

                                $response = [
                                        'request_id'    => $request->request_id,
                                        'response_id'   => $response_id,
                                        'response_code'     => "00",
                                        'response_message'  => 'Login Success!',
                                        'data'      => [
                                                'token'                 => $token,
                                        ]
                                ];
                                $http_respon = Response::HTTP_OK;
                        }
                }else{
                        $response = [
                                'request_id'    => $request->request_id,
                                'response_id'   => $response_id,
                                'response_code'     => "99",
                                'response_message'  => 'Login gagal!'
                        ];
                        $http_respon = Response::HTTP_OK;
                }

                } catch (QueryException $th) {
                        return response()->json(['success' => false, 'message' => "Failed " . $th]);
                }


                return response()->json($response, $http_respon);
        }

        public function logout(Request $request)
        {
                $input = $request->all();
                $response_id = Uuid::uuid4()->toString();
                $validator = Validator::make($request->all(), [
                        'request_id'    => 'required'
                ]);

                if ($validator->fails()) {
                        return response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
                }

                $bearerToken        = $request->bearerToken();
                $time               = Carbon::now();
                $current_timestamp  = $time->toDateTimeString();
                $id                 = $this->getUserID($bearerToken);
                try {
                        $result = DB::table('oauth_access_tokens')
                                ->where('user_id', '=', $id)
                                ->where('revoked', '=', 0)
                                ->update(['revoked' => 1, 'updated_at' => $current_timestamp, 'expires_at' =>  $current_timestamp]);

                        $session = DB::table('users')
                                ->where('remember_token', '=', $bearerToken)
                                ->update(['remember_token' => null]);
                        $response = [
                                'response_code'     => '00',
                                'response_message'  => 'Logout successfully.',
                                'request_id'        => $request->request_id,
                                'response_id'       => $response_id
                        ];
                        $http_respon = Response::HTTP_OK;
                } catch (GuzzleException $th) {
                        $response = [
                                'response_code'     => '99',
                                'response_message'  => 'Failed'
                        ];
                        $http_respon = Response::HTTP_BAD_REQUEST;
                }
                return response()->json($response, $http_respon);
        }


        public function index(Request $request)
        {
                $input              = $request->all();
                $response_id        = Uuid::uuid4()->toString();

                $validator = Validator::make($request->all(), [
                        'request_id' => 'required'
                ]);

                if ($validator->fails()) {
                        return response()->json($validator->errors(), HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
                }

                $bearerToken = $request->bearerToken();


                $id_request = $request->request_id;
                $id_respon = $this->UUID();


                if (empty($bearerToken)) {
                        $response = [
                                'request_id' =>  $id_request,
                                'response_id' => $id_respon,
                                'response_message' => 'General Failure - Tidak ada token',
                                'response_code' => '99',
                        ];
                } else {

                        $data_user = User::get();
                        $user = array();
                        $d=0;
                        foreach($data_user as $key => $val)
                        {
                                $user[$d]['id']         = $val->id;
                                $user[$d]['username']   = $val->username;
                                $user[$d]['created_at']   = $val->created_at;
                                $user[$d]['updated_at']   = $val->updated_at;

                                $d++;
                        }

                        $response = [
                                'request_id' =>  $id_request,
                                'response_id' => $id_respon,
                                'response_message' => 'Success',
                                'response_code' => '00',
                                'data' => $user
                        ];

                        return response()->json($response, HttpResponse::HTTP_OK);
                }



        }

        public function store(Request $request)
        {
                $validator = Validator::make($request->all(), [
                        'request_id' => 'required'
                ]);

                if ($validator->fails()) {
                        return response()->json($validator->errors(), HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
                }

                $bearerToken = $request->bearerToken();


                $id_request = $request->request_id;
                $id_respon = $this->UUID();


                if (empty($bearerToken)) {
                        $response = [
                                'request_id' =>  $id_request,
                                'response_id' => $id_respon,
                                'response_message' => 'General Failure - Tidak ada token',
                                'response_code' => '99',
                        ];
                } else {

                        $username      =       $request->data['username'];
                        $password       =       Hash::make($request->data['password']);

                        $check = User::where('username',$username)->first();
                        if(!empty($check))
                        {
                                $response = [
                                        'request_id' =>  $id_request,
                                        'response_id' => $id_respon,
                                        'response_message' => 'General Failure - Data Sudah ada',
                                        'response_code' => '99'
                                ];
                                return response()->json($response, HttpResponse::HTTP_OK);

                        }
                        try {
                                $user = User::create([
                                        'username'       =>      $username,
                                        'password'       =>      $password,
                                        'created_by'     =>      Carbon::now(),
                                        'updated_by'     =>      Carbon::now()
                                ]);

                                if($user)
                                {
                                        DB::commit();
                                        $response = [
                                                'request_id' =>  $id_request,
                                                'response_id' => $id_respon,
                                                'response_message' => 'Success',
                                                'response_code' => '00'
                                        ];
                                }else{
                                        DB::rollBack();
                                        $response = [
                                                'request_id' =>  $id_request,
                                                'response_id' => $id_respon,
                                                'response_message' => 'General Failure -Query error',
                                                'response_code' => '99'
                                        ];
                                }
                        }catch (QueryException $e) {

                                $response = [
                                        'request_id' =>  $id_request,
                                        'response_id' => $id_respon,
                                        'response_message' => 'General Failure -Query error' . $e,
                                        'response_code' => '99'
                                ];

                                return response()->json($response, HttpResponse::HTTP_OK);
                        }
                }

                return response()->json($response, HttpResponse ::HTTP_OK);
        }
}
