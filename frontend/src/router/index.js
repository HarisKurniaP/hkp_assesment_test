import Vue from 'vue'
import VueRouter from 'vue-router'
 
Vue.use(VueRouter)
 
  const routes = [
  
  {
    path: '/',
    name: 'login',
    component: () => import('../components/Login.vue')
  },
  {
    path: '/registration',
    name: 'registration',
    component: () => import('../components/Registration.vue')
  },
  {
    path: '/DataTable',
    name: 'DataTable',
    component: () => import('../components/DataTable.vue')
  }
]
 
const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
 
export default router